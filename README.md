# Гайд по безопасности

## Что это такое?

Краткое описание технологий и инструментов которые мы применяем в работе для обеспечения безопасности linux хостов и сохранности данных пользователей. При выборе инструментов обеспечения безопасности мы руководствуемся здравым смыслом и лучшими практиками доступными сообществу.

## Что мы делаем?

* Мы используем подход Configuration as code и в ближайшее время начнём применять Infrastructure as code, для того чтобы иметь возможность понимать что изменяется в нашей инфраструктуре, кем и с какой целью.
Для этого мы используем инструменты автоматизации: ansible, gitlab

* Мы хотим тестировать применённую конфигурацию на соответствие разумным требованиям безопасности данных и хостов, предоставляемыми нашим бизнесом, согласовывая их с общепринятыми базовыми требованиями от сообщества.
Поэтому мы применяем инструмент тестирования конфигурации [Chef Inspec](inspec.io) и следим за развитием проекта [DevSec Hardening Framework](dev-sec.io)

#### Список импортированных и применённых профилей для Chef Inspec:

|профиль | источник | дата начала использования |
|---|---|---|
| linux baseline | [linux-baseline](/dev-sec/linux-baseline) | 10 Aug 2020|
| ssh-baseline |[ssh-baseline](/dev-sec/ssh-baseline)| 10 Aug 2020|
| docker-baseline |[docker-baseline](/dev-sec/docker-baseline)| 1 Sep 2020 |
| bestdoctor-inhouse-profile|WIP||

* Мы хотим следить за базами уязвимостей/уязвимых пакетов в используемых дистрибутивах и своемременно обновлять ПО.
Поэтому мы атоматизировали эти процессы и мы получаем еженедельне отчёты о состоянии установленных пакетов с помощью [Vuls](vuls.io). После получения отчёта мы применяем патчи/устанавливаем критические обновления по расписанию.
